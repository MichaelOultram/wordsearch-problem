#!/usr/bin/env python
import random

class CacheTable(object):
    def __init__(self, max_level, _level=0):
        """A multi-level cache for storing words

        Parameters
        ----------
        max_level : int
            Number of cache levels to use.

        _level : int
            The index of the word that this CacheTable deals with
        """
        assert(0 <= _level <= max_level)
        self._level = _level
        self._table = {} if _level < max_level else []
        self.max_level = max_level

    def insert(self, elem):
        """Inserts an element into the CacheTable

        Parameters
        ----------
        elem : str
            Word to insert

        Returns
        -------
        bool
            True if successful, False otherwise.
        """

        if len(elem) == self._level:
            return True

        if self.max_level == self._level:
            self._table.append(elem[self._level:])
            self._table.sort() # TODO: Take advantage of knowing the list is sorted
            return True
        else:
            index = elem[self._level]
            if index not in self._table:
                # Next level doesn't exist yet. Create it
                self._table[index] = CacheTable(self.max_level, self._level + 1)
            # Pass the elem onto the next level
            return self._table[index].insert(elem)


    def contains(self, word, comparison=None):
        """Checks to see if any element in the cache matches the given word

        Parameters
        ----------
        word : str
            The word to check

        comparison : str, str -> bool
            The function used for comparing whether two words are equal. Default comparison is equality

        Returns
        -------
        bool
            True if an element exists in the CacheTable. False otherwise
        """

        # Default comparison function is ==
        if comparison is None:
            comparison = lambda a, b: a == b

        if len(word) == self._level:
            # If we exist, then word must exist in the cache
            return True

        elif self.max_level == self._level:
            # Binary Search for word
            word = word[self._level:]
            startPoint = 0
            endPoint = len(self._table) - 1
            while startPoint <= endPoint:
                midPoint = int((startPoint + endPoint) / 2)
                table_word = self._table[midPoint]
                if comparison(table_word, word):
                    return True
                elif table_word < word:
                    startPoint = midPoint + 1
                elif table_word > word:
                    endPoint = midPoint - 1
                else:
                    assert(False) # Should be impossible
            return False

        else:
            next_char = word[self._level]
            if next_char not in self._table:
                return False
            # Pass the word onto the next level
            return self._table[next_char].contains_starts_with(word)


    def contains_starts_with(self, prefix):
        """Checks to see if any element in the cache starts with the passed prefix

        Parameters
        ----------
        prefix : str
            The prefix to check

        Returns
        -------
        bool
            True if an element exists in the CacheTable with the given prefix. False otherwise
        """
        return self.contains(prefix, lambda a, b: a.startswith(b))


    def __str__(self):
        return "{}".format(self._table)
    __repr__ = __str__


class WordSearch:
    def __init__(self, grid, ROW_LENGTH=10000,
                             MAX_WORD_LENGTH=24,
                             NUM_CACHE_LEVELS=5):
        """A solver that can detect whether a word is contained within a wordsearch

        Parameters
        ----------
        grid : str
            The wordsearch grid with all lines concatinated together with no separator

        ROW_LENGTH : int
            The length of one row of the wordsearch

        MAX_WORD_LENGTH : int
            The maximum length of a word in the wordsearch. Any word longer than this value will not be detected by ``is_present``

        NUM_CACHE_LEVELS : int
            The number of levels to use in the ``CacheTable``

        Notes
        -----
        The length of grid must be a multiple of ROW_LENGTH.

        Increasing MAX_WORD_LENGTH or the size of the grid will significantly increase memory consumption.
        """

        # Check that grid is uniform
        assert(len(grid) % ROW_LENGTH == 0)

        max_x = ROW_LENGTH
        max_y = int(len(grid) / ROW_LENGTH)

        # Gather all lines from the grid
        all_lines = []
        # Get all rows from grid
        for y in range(max_y):
            row_start = y * ROW_LENGTH
            row_end = (y + 1) * ROW_LENGTH
            row = grid[row_start:row_end]
            all_lines.append(row)
        # Get all columns from grid
        for x in range(max_x):
            column = []
            for y in range(max_y):
                column.append(grid[y*ROW_LENGTH + x])
            all_lines.append(column)

        # Create an empty multi-level table to insert words from the grid into
        self._num_cache_levels = NUM_CACHE_LEVELS
        self._cache_table = CacheTable(NUM_CACHE_LEVELS)

        # Scan across each line and insert it into the cache
        for line in all_lines:
            for i in range(len(line)):
                # Subdivide the word
                word = ''.join(line[i:i+MAX_WORD_LENGTH])
                self._cache_table.insert(word)


    def is_present(self, word):
        """Checks to see whether the word is contained within the grid

        Parameters
        ----------
        word: str
            The word to find in the grid

        Returns
        -------
        bool
            True if the word is found. False otherwise

        Notes
        -----
        Words longer than MAX_WORD_LENGTH will not be found.
        """
        rev_word = str(word[::-1])
        forward = self._cache_table.contains_starts_with(word)
        backwards = self._cache_table.contains_starts_with(rev_word)
        return forward or backwards


def random_wordsearch(ROW_LENGTH=1000,
                      MAX_WORD_LENGTH=24,
                      NUM_CACHE_LEVELS=5):
    """Creates a wordsearch with a grid of random characters

    Parameters
    ----------
    width : int
        Width of the wordsearch to generate

    Returns
    -------
    WordSearch
        A wordsearch made from random characters
    """
    alphabet = [chr(i + 97) for i in range(26)]
    print("Creating Grid")
    grid = "".join([random.choice(alphabet) for i in range(ROW_LENGTH * ROW_LENGTH)])
    print("Creating Wordsearch")
    return WordSearch(grid, ROW_LENGTH=ROW_LENGTH,
                            MAX_WORD_LENGTH=MAX_WORD_LENGTH,
                            NUM_CACHE_LEVELS=NUM_CACHE_LEVELS)
if __name__ == "__main__":
    ws = random_wordsearch()
    print("Generating Words")

    def genword():
        alphabet = [chr(i + 97) for i in range(26)]
        wordlength = random.randint(3, 24)
        return "".join([random.choice(alphabet) for i in range(wordlength)])

    wordlist = [genword() for wordid in range(1000000)]
    print("Testing Words")
    for word in wordlist:
        print("{}: {}".format(ws.is_present(word), word))
