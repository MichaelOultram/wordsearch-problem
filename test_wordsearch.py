#!/usr/bin/env python
from wordsearch import WordSearch, CacheTable
import random

def test_6x6():
    # From: http://www.activityshelter.com/wp-content/uploads/2016/12/easy-word-search-for-kids-printable.png
    grid = "mdaerbsbagelpnmznwotlebuoubirdnblleb"
    ws = WordSearch(grid, ROW_LENGTH=6,
                          MAX_WORD_LENGTH=5,
                          NUM_CACHE_LEVELS=4)
    print(ws)
    words_in_grid = ["bell", "belt", "bird", "bread", "bagel", "spoon"]
    for word in words_in_grid:
        assert(ws.is_present(word))

    words_not_in_grid = ["turtle", "word", "bad"]
    for word in words_not_in_grid:
        assert(not ws.is_present(word))


def test_cache_table():
    # Words from: https://myvocabulary.com/word-list/squirrels-vocabulary/
    for max_level in range(5, 10):
        out_words = ["acorns", "action", "adaptation", "albino", "availability", "backyard",
                 "birdfeeders", "birdseed", "black", "brown", "burrow", "bushy",
                 "cache", "chipmunk", "climbing", "collect", "crack", "crumbs",
                 "destruction", "digits", "everywhere", "explore", "feeding", "female",
                 "fields", "flyingsquirrels", "forage", "forest", "frontfeet", "frontyard",
                 "fruit", "fungi", "glide", "groundsquirrels", "groundhog", "group",
                 "habitat", "incisor", "inhibit", "insects", "international", "leaves",
                 "location", "marmot", "munch", "national", "nibble", "nocturnal",
                 "notice", "nuisance", "numbers", "numerous", "nutrient", "observe",
                 "parks", "pests", "pinecone", "place", "plantshoots", "playful",
                 "population", "prairiedog", "rabid", "rascals", "region", "relationship",
                 "rodent", "roots", "science", "search", "smell", "species", "squirrels",
                 "tails", "threat", "treesquirrels", "variation", "watch", "worldwide", "zigzag"]
        in_words = []
        random.shuffle(out_words)

        print("Creating CacheTable with {} level(s)".format(max_level))
        cache_table = CacheTable(max_level)
        for i in range(len(out_words)):
            # Check that all out_words are out
            for word in out_words:
                assert(not cache_table.contains_starts_with(word))

            # Insert a new word
            new_word = out_words.pop()
            print("Inserting {}".format(new_word))
            assert(cache_table.insert(new_word))
            in_words.append(new_word)

            # Check that all in_words are in
            for word in in_words:
                # Check all prefixes are also found for each word
                for length in range(1, len(word) + 1):
                    subword = word[:length]
                    assert(cache_table.contains_starts_with(subword))
        print()
